# Hausarbeit CompPhys

Hausarbeit zu zweidimensionalen Ising-Modell im Rahmen der Lehrveranstaltung "Computational Physics" an der TU Braunschweig im WiSe 2020/21.

Dieses Projekt wurde mit und für Kaggle entwickelt. Es ist *nicht* lokal lauffähig.